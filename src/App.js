import React from 'react';  
import logo from './logo.svg';  
import './App.css';  
import Layout  from './Layout/Layout'  
import Homepage from './Layout/Homepage' 

import { HashRouter, Route, Switch } from 'react-router-dom';  
function App() {  
  return (  
     <div className="App">  
      <Homepage/>   
     </div>  
  );  
}  

export default App; 